from bayes_opt import BayesianOptimization
import subprocess
import pandas as pd
import numpy as np
from numpy import genfromtxt
import matplotlib.pyplot as plt
from sacred import Experiment
from sacred.observers import FileStorageObserver

ex = Experiment('my_experiment')
ex.observers.append(FileStorageObserver('my_runs'))

def execute_command(bashCommand):
    process = subprocess.Popen(bashCommand, stdout=subprocess.PIPE)
    output, error = process.communicate()

def read_experimental():
    data = genfromtxt('./validation/exptData/m01_094l_01.his_lin.dx0.5.mapped.dat')
    return data[:, 0], data[:, 1]/(np.sum(data[:, 1])*0.5)

def read_results():
    data = genfromtxt('./postProcessing/volumeDensity/3/volumeDensity.dat', skip_header=3, delimiter='\t')
    return data[:, 2]/1e-3, data[:, 3]/1e3

@ex.capture
def black_box_function(BTurb, BShear, BEddy, BFric, CEff, CTurb, CBuoy, CShear, CEddy, CWake):
    """Function with unknown internals we wish to maximize.

    This is just serving as an example, for all intents and
    purposes think of the internals of this function, i.e.: the process
    which generates its output values, as unknown.
    """

    m = "(Liao{turbulence yes;laminarShear yes;turbulentShear yes;interfacialFriction yes;BTurb " + str(BTurb) + ";BShear " + str(BShear) + ";BEddy " + str(BEddy) + ";BFric " + str(BFric) + ";})"
    cl = ['foamDictionary', '-entry', 'populationBalanceCoeffs/bubbles/binaryBreakupModels', '-set', m, './constant/phaseProperties']

    m2 = "(Liao{turbulence yes; buoyancy yes; laminarShear yes; eddyCapture yes; wakeEntrainment yes; CEff " + str(CEff) + "; CTurb " + str(CTurb) + "; CBuoy " + str(CBuoy) + "; CShear" + str(CShear) + "; CEddy" + str(CEddy) + "; CWake" + str(CWake) + ";})"
    cl2 = ['foamDictionary', '-entry', 'populationBalanceCoeffs/bubbles/binaryBreakupModels', '-set', m, './constant/phaseProperties']


    execute_command('./Allclean')
    execute_command(cl)
    execute_command(cl2)
    execute_command('./Allrun')
    sizes, fractions = read_experimental()
    sizes2, fractions2 = read_results()
    error = -np.sum(np.abs(fractions-fractions2))
    ex.log_scalar("run", {'BTurb': BTurb, 'BShear': BShear, 'BEddy': BEddy, 'BFric': BFric, 'error': -error}) 
    return error





@ex.automain
def my_main():
    # Bounded region of parameter space
    pbounds = {
        'BTurb': (0.3, 10),
        'BShear': (0.3, 10),
        'BEddy': (0.3, 10),
        'BFric': (0.3, 10),
        'CEff': (0.3, 10),
        'CTurb': (0.3, 10),
        'CBuoy': (0.3, 10),
        'CShear': (0.3, 10),
        'CEddy': (0.3, 10),
        'CWake': (0.3, 10)

    }

    optimizer = BayesianOptimization(
        f=black_box_function,
        pbounds=pbounds
    )

    optimizer.probe(
        params={'BTurb': 1.0, 'BShear': 1.0, 'BEddy': 1.0, 'BFric': 1.0, 'CEff': 5.0, 'CTurb': 1.0, 'CBuoy': 1.0, 'CShear': 1.0, 'CEddy': 1.0, 'CWake': 1.0},
        lazy=True,
    )


    optimizer.maximize(
        init_points=20,
        n_iter=2000,
    )

    print(optimizer.max)
