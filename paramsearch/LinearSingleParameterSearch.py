from collections import defaultdict
import numpy as np
import matplotlib.pyplot as plt
from inspect import ismethod
import argparse
import yaml
from paramsearch.OpenFOAMExperiment import OpenFOAMExperiment


def method_exists(instance, method):
    return hasattr(instance, method) and ismethod(getattr(instance, method))


class LinearSingleParameterSearch:
    def __init__(self, experiment, range_splits=10):
        self.experiment = experiment
        self.range_splits = range_splits
        self._search_space = defaultdict(lambda: [])
        self._search_defaults = self.experiment.get_default_values().copy()

    def run(self):
        if method_exists(self.experiment, "start_experiment"):
            self.experiment.start_experiment()
        for param in self.experiment.get_variables():
            default_parameters = self.experiment.get_default_values().copy()
            parameter_ranges = self.experiment.get_ranges()
            start_range, end_range = parameter_ranges[param]
            for value in np.linspace(start_range, end_range, self.range_splits):
                default_parameters[param] = value
                try:
                    self.experiment.pre_evaluate(**default_parameters)
                except AttributeError:
                    pass
                fitness_value = self.experiment.evaluate(**default_parameters)
                try:
                    self.experiment.post_evaluate(**default_parameters)
                except AttributeError:
                    pass
                self._search_space[param].append(fitness_value)
        if method_exists(self.experiment, "stop_experiment"):
            self.experiment.stop_experiment()

    def visualize(self, folder='./linear_search'):
        import os
        if not os.path.exists(folder):
            os.makedirs(folder)
        fig, ax_list = plt.subplots(len(self._search_space))

        for i, (key, value) in enumerate(self._search_space.items()):
            ax_list[i].plot(value)
        fig.savefig(os.path.join(folder, 'viz.pdf'))


def main():
    parser = argparse.ArgumentParser(prog='LS')
    parser.add_argument('-f', type=str, required=True, help='yaml file')
    args = parser.parse_args()

    with open(args.f) as f:
        data = yaml.load(f, Loader=yaml.FullLoader)

    exp = OpenFOAMExperiment(data['search']['params'], command=data['search']['commands']['command'])
    print(data['search']['algorithm']['params'])
    s = LinearSingleParameterSearch(exp, **data['search']['algorithm']['params'])
    s.run()
    s.visualize()


if __name__ == "__main__":
    main()
