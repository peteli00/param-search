from unittest import TestCase
from unittest.mock import Mock, MagicMock
from paramsearch.LinearSingleParameterSearch import LinearSingleParameterSearch


class TestLinearSingleParameterSearch(TestCase):
    def setUp(self):
        params = ['BTurb', 'BShear', 'BEddy']
        self.mock = Mock()
        self.mock.evaluate = MagicMock(return_value=3)
        self.mock.get_variables = MagicMock(return_value=params)
        self.mock.get_ranges = MagicMock(return_value={x: (0.1, 10) for x in params})
        self.mock.get_default_values = MagicMock(return_value={x: 1 for x in params})

    def test_number_of_calls_1(self):
        ls = LinearSingleParameterSearch(self.mock)
        ls.run()
        self.assertEqual(self.mock.evaluate.call_count, 30)
        self.assertEqual(self.mock.pre_evaluate.call_count, 30)
        self.assertEqual(self.mock.post_evaluate.call_count, 30)

    def test_number_of_calls_2(self):
        self.mock.pre_evaluate = MagicMock()
        self.mock.post_evaluate = MagicMock()
        ls = LinearSingleParameterSearch(self.mock, 100)
        ls.run()
        self.assertEqual(self.mock.evaluate.call_count, 300)
        self.assertEqual(self.mock.pre_evaluate.call_count, 300)
        self.assertEqual(self.mock.post_evaluate.call_count, 300)
