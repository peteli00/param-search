from paramsearch.Experiment import Experiment


class TestExperiment(Experiment):
    def __init__(self):
        pass

    def get_variables(self):
        return ['BTurb', 'BShear', 'BEddy']

    def get_ranges(self):
        return {'BTurb': (0.1, 10), 'BShear': (0.1, 10), 'BEddy': (0.1, 10)}

    def get_default_values(self):
        return {'BTurb': 1, 'BShear': 1, 'BEddy': 1}

    def evaluate(self, BTurb, BShear, BEddy):
        return (BTurb-5)**2 + BShear**2 + BEddy

    def pre_evaluate(self, **kwargs):
        pass
