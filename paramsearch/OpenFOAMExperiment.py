import subprocess
import numpy as np
from numpy import genfromtxt


class OpenFOAMExperiment:
    def __init__(self, params, command):
        self.ranges = {}
        self.default_values = {}
        for param in params:
            self.ranges[param['name']] = (float(param['range']['start']), float(param['range']['end']))
            self.default_values[param['name']] = param['default']
        self.command = command

    def __execute_command(self, bashCommand):
        process = subprocess.Popen(bashCommand, stdout=subprocess.PIPE)
        output, error = process.communicate()

    def get_variables(self):
        return self.ranges.keys()

    def get_ranges(self):
        return self.ranges

    def get_default_values(self):
        return self.default_values

    def read_experimental(self):
        data = genfromtxt('./validation/exptData/m01_094l_01.his_lin.dx0.5.mapped.dat')
        return data[:, 0], data[:, 1] / (np.sum(data[:, 1]) * 0.5)

    def read_results(self):
        data = genfromtxt('./postProcessing/volumeDensity/3/volumeDensity.dat', skip_header=3, delimiter='\t')
        return data[:, 2] / 1e-3, data[:, 3] / 1e3

    def evaluate(self, **par):
        self.__execute_command('./Allclean')
        for i in range(len(self.command)):
            command_part = self.command[i]
            for k, v in par.items():
                command_part = command_part.replace('{' + k + '}', str(v))
            self.command[i] = command_part
        self.__execute_command(self.command)
        self.__execute_command('./Allrun')
        sizes, fractions = self.read_experimental()
        sizes2, fractions2 = self.read_results()
        self.__execute_command('./Allclean')
        error = np.sum(np.abs(fractions - fractions2))
        print(par, error)
        return error
