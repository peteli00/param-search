#
# Customized development container for OpenFOAM
#

# Build variables required for docker image
ARG FROMIMAGE

FROM $FROMIMAGE

# Add your name and your e-mail adress to identify container maintainer
LABEL maintainer="Petelin, Gasper (FWDC, g.petelin@hzdr.de)" \
    description="Customized development container for OpenFOAM."

# Install custom ubuntu packages
RUN apt-get update \
    && apt-get install -qy --fix-missing --no-install-recommends \
    geany

COPY . /app
# Instal package
RUN pip3 install /app/.

# Slim down packages
RUN apt-get clean autoclean && apt-get autoremove -y
