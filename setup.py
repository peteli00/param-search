import setuptools

with open("./README.md", "r") as fh:
    long_description = fh.read()

packages = [
    'numpy',
    'matplotlib',
    'pandas',
    'bayesian-optimization',
    'sacred',
    'pyyaml'
    ]

setuptools.setup(
    name="paramsearch",
    version="0.0.1",
    description="Some description",
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=setuptools.find_packages(),
    install_requires=packages,
    tests_require=[
            'mock'
        ],
    entry_points={
        'console_scripts': [
            'search = paramsearch.LinearSingleParameterSearch:main',
        ],
    }
)
